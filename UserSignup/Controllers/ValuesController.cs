﻿using System;
using System.IO;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using UserSignup.Web.Models;
using FileWriter = System.IO.File;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UserSignup.Web.Controllers
{
    [Route("api/Values"), Produces("application/json"), EnableCors("AppPolicy")]
    public class ValuesController : Controller
    {
        [HttpPost, Route("PostUser")]
        public IActionResult PostUser([FromBody]User model)
        {
            try
            {
                string jsonRepresentation = JsonConvert.SerializeObject(model);
                string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "output.txt");
                FileWriter.AppendAllText(filePath, jsonRepresentation);

                return Accepted();
            }
            catch (Exception e)
            {
                return BadRequest("Something went wrong in processing the request. Error message " + e.Message +
                                  "\n StackTrace: " + e.StackTrace);
            }
        }
    }
}
