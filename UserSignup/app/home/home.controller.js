templatingApp.controller('HomeController', ['$scope', '$http', function($scope, $http) {
    $scope.userModel = {};
    $scope.userModel.id = 0;
    $http.errormessage = {
        show: false
    };

    $scope.errormessage = $http.errormessage;

    //******=========Save User=========******
    $scope.saveUser = function () {
        $http({
            method: 'POST',
            url: '/api/Values/PostUser/',
            data: $scope.userModel
        }).then(function (response) {
            $scope.reset();
        }, function (error) {
            $scope.showError();
        });
    };

    $scope.reset = function () {
        $scope.userModel = {};
        $scope.userModel.id = 0;
        $scope.errormessage.show = false;
    };

    $scope.showError = function() {
        $http.errormessage.show = true;
    };

}]);
